package at.fhhgb.ai;

/**
 * This is a template for the class corresponding to your original
 * advanced heuristic.  This class is an implementation of the
 * <tt>Heuristic</tt> interface.  After thinking of an original
 * heuristic, you should implement it here, filling in the constructor
 * and the <tt>getValue</tt> method.
 */
public class AdvancedHeuristic implements Heuristic {
    private Puzzle puzzle;
    private final int numCars;
    private final int carSize;
    private final int carFixedPos;

    /**
     * This is the required constructor, which must be of the given form.
     */
    public AdvancedHeuristic(Puzzle puzzle) {
        this.puzzle = puzzle;
        this.numCars = puzzle.getNumCars();
        this.carSize = puzzle.getCarSize(0);
        this.carFixedPos = puzzle.getFixedPosition(0);
    }

    /**
     * This method returns the value of the heuristic function at the
     * given state.
     */
    public int getValue(State state) {
        // If goal return 0
        if (state.isGoal()) {
            return 0;
        }

        // Always start with 1 if it is not the goal
        int blockingCars = 1;
        for (int i = 1; i < this.numCars; i++) {
            // Check if car is blocking our car
            if (isBlockingCar(i, state)) {
                blockingCars++;
                // Check if blocking car can be moved, if not add 1 otherwise 0
                blockingCars += canCarBeMoved(i, state) ? 0 : 1;
            }
        }

        return blockingCars;
    }

    /**
     * Checks if specified car is blocking our car
     *
     * @param blockingCarIndex index of the car
     * @param state            state
     * @return true, if car is blocking, false if not
     */
    private boolean isBlockingCar(int blockingCarIndex, State state) {
        return this.puzzle.getCarOrient(blockingCarIndex) && // only vertical aligned cars are blocking
                this.puzzle.getFixedPosition(blockingCarIndex) > (state.getVariablePosition(0) + this.carSize - 1) && // check if car is behind ours (horizontal)
                this.carFixedPos >= state.getVariablePosition(blockingCarIndex) && // check if car is between ours (vertical)
                this.carFixedPos <= (state.getVariablePosition(blockingCarIndex) + this.puzzle.getCarSize(blockingCarIndex) - 1); // check if car is between ours (vertical)
    }

    /**
     * Checks if specified car can be moved
     *
     * @param carIndex index of the car which is looked up
     * @param state    state
     * @return true, if can be moved, false if can't
     */
    private boolean canCarBeMoved(int carIndex, State state) {
        // check if it can be moved back
        if (this.carFixedPos - this.puzzle.getCarSize(carIndex) >= 0) {
            // check if there is enough space in the grid
            for (int i = this.carFixedPos - 1; i >= this.carFixedPos - this.puzzle.getCarSize(carIndex); i--) {
                int carInGridIndex = state.getGrid()[this.puzzle.getFixedPosition(carIndex)][i];
                if (carInGridIndex > -1 && carInGridIndex != carIndex) {
                    return false;
                }
            }
            return true;
        }
        // check if it can be moved front
        if (this.carFixedPos + this.puzzle.getCarSize(carIndex) < this.puzzle.getGridSize()) {
            // check if there is enough space in the grid
            for (int i = this.carFixedPos + 1; i <= this.carFixedPos + this.puzzle.getCarSize(carIndex); i++) { // check if it can be moved back
                int carInGridIndex = state.getGrid()[this.puzzle.getFixedPosition(carIndex)][i];
                if (carInGridIndex > -1 && carInGridIndex != carIndex) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
