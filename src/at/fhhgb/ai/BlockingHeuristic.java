package at.fhhgb.ai;

/**
 * This is a template for the class corresponding to the blocking
 * heuristic.  This heuristic returns zero for goal states, and
 * otherwise returns one plus the number of cars blocking the path of
 * the goal car to the exit.  This class is an implementation of the
 * <tt>Heuristic</tt> interface, and must be implemented by filling in
 * the constructor and the <tt>getValue</tt> method.
 */
public class BlockingHeuristic implements Heuristic {
    private Puzzle puzzle;
    private final int numCars;
    private final int carSize;
    private final int carFixedPos;

    /**
     * This is the required constructor, which must be of the given form.
     */
    public BlockingHeuristic(Puzzle puzzle) {
        this.puzzle = puzzle;
        this.numCars = puzzle.getNumCars();
        this.carSize = puzzle.getCarSize(0);
        this.carFixedPos = puzzle.getFixedPosition(0);
    }


    /**
     * This method returns the value of the heuristic function at the
     * given state.
     */
    public int getValue(State state) {
        // If goal return 0
        if (state.isGoal()) {
            return 0;
        }

        // Always start with 1 if it is not the goal and add cars which are blocking our car
        int blockingCars = 1;
        for (int i = 1; i < this.numCars; i++) {
            // Check if car is blocking our car
            if (isBlockingCar(i, state)) {
                blockingCars++;
            }
        }

        return blockingCars;
    }

    /**
     * Checks if specified car is blocking our car
     *
     * @param blockingCarIndex index of the car
     * @param state            state
     * @return true, if car is blocking, false if not
     */
    private boolean isBlockingCar(int blockingCarIndex, State state) {
        return this.puzzle.getCarOrient(blockingCarIndex) && // only vertical aligned cars are blocking
                this.puzzle.getFixedPosition(blockingCarIndex) > (state.getVariablePosition(0) + this.carSize - 1) && // check if car is behind ours (horizontal)
                this.carFixedPos >= state.getVariablePosition(blockingCarIndex) && // check if car is between ours (vertical)
                this.carFixedPos <= (state.getVariablePosition(blockingCarIndex) + this.puzzle.getCarSize(blockingCarIndex) - 1); // check if car is between ours (vertical)
    }
}
