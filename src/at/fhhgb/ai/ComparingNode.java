package at.fhhgb.ai;

public class ComparingNode extends Node implements Comparable<ComparingNode> {

    /**
     * Used for FIFO
     */
    public final long timeStamp;
    /**
     * Used for comparing the value between nodes
     */
    private Heuristic heuristic;

    /**
     * The main constructor for constructing a search node.  You
     * probably will never need to use this constructor directly,
     * since ordinarily, new nodes will be gotten from the
     * <tt>expand</tt> method.
     *
     * @param state     the state that resides at this node
     * @param depth     the search depth of this node
     * @param parent    the parent of this node
     * @param heuristic the heuristic which is used for comparing
     */
    public ComparingNode(State state, int depth, Node parent, Heuristic heuristic) {
        super(state, depth, parent);
        this.heuristic = heuristic;
        this.timeStamp = System.nanoTime();
    }

    public ComparingNode(Node node, Heuristic heuristic) {
        super(node.getState(), node.getDepth(), node.getParent());
        this.heuristic = heuristic;
        this.timeStamp = System.nanoTime();
    }

    @Override
    public int hashCode() {
        return this.getState().hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other || other == null) {
            return false;
        }

        if (other instanceof Node || other instanceof ComparingNode) {
            return ((Node) other).getState().equals(this.getState());
        }

        return false;
    }

    @Override
    public int compareTo(ComparingNode o) {
        // total costs > FIFO
        int comparingValue = (heuristic.getValue(getState()) + getDepth()) - (heuristic.getValue(o.getState()) + o.getDepth());
        if (comparingValue == 0) {
            return timeStamp < o.timeStamp ? -1 : 1; // FIFO
        }
        return comparingValue;
    }
}
