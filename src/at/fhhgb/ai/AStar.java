package at.fhhgb.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

/**
 * This is the template for a class that performs A* search on a given
 * rush hour puzzle with a given heuristic.  The main search
 * computation is carried out by the constructor for this class, which
 * must be filled in.  The solution (a path from the initial state to
 * a goal state) is returned as an array of <tt>State</tt>s called
 * <tt>path</tt> (where the first element <tt>path[0]</tt> is the
 * initial state).  If no solution is found, the <tt>path</tt> field
 * should be set to <tt>null</tt>.  You may also wish to return other
 * information by adding additional fields to the class.
 */
public class AStar {

    private PriorityQueue<ComparingNode> openList;
    private List<ComparingNode> closedList;

    /**
     * The solution path is stored here
     */
    public State[] path;

    /**
     * This is the constructor that performs A* search to compute a
     * solution for the given puzzle using the given heuristic.
     */
    public AStar(Puzzle puzzle, Heuristic heuristic) {
        // create lists
        openList = new PriorityQueue<>();
        closedList = new ArrayList<>();

        ComparingNode currentNode = new ComparingNode(puzzle.getInitNode(), heuristic);
        openList.add(currentNode);

        // go through nodes until the path is found
        while (!currentNode.getState().isGoal()) {
            // check if no solution was found
            if (openList.isEmpty()) {
                this.path = null;
                return;
            }
            // get new node
            currentNode = openList.poll();
            closedList.add(currentNode);

            for (Node expandedNode : currentNode.expand()) {
                ComparingNode expandedCompNode = new ComparingNode(expandedNode, heuristic);
                // check if node is not in closed list
                if (!closedList.contains(expandedCompNode)) {
                    // see if node is already in openList, if yes prune
                    if (openList.contains(expandedCompNode)) {
                        prune(expandedCompNode, openList);
                    } else {
                        openList.add(expandedCompNode);
                    }
                }
            }
        }

        // Getting path to goal
        this.setPath(currentNode);
    }

    /**
     * Replaces a node on the list if it is better in terms of value (pruning)
     *
     * @param node node which should be added to list
     * @param list list which will be searched for
     */
    private void prune(ComparingNode node, PriorityQueue<ComparingNode> list) {
        for (ComparingNode listNode : list) {
            if (listNode.equals(node)) {
                if (node.compareTo(listNode) < 0) {
                    list.remove(listNode);
                    list.add(node);
                }
                break;
            }
        }
    }

    /**
     * Sets the path array of the goal node
     *
     * @param goalNode
     */
    private void setPath(Node goalNode) {
        List<State> states = new ArrayList<>();
        do {
            states.add(goalNode.getState());
            goalNode = goalNode.getParent();
        } while (goalNode != null);
        Collections.reverse(states);
        this.path = new State[states.size()];
        this.path = states.toArray(this.path);
    }
}
